import Koa from "koa";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";
import Queue from "bee-queue";
import { jobExists, setJob } from "./service/redis/job";

const crawlersQueue = new Queue<CrawlerQueueDataType>("crawlers", {
  redis: {
    host: "redis",
  },
  isWorker: false,
});

const app = new Koa();
const router = new Router();

app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);

router.get("home", "/", (ctx) => {
  ctx.body = { home: true };
});

router.post("add", "/add", async (ctx) => {
  const body = ctx.request.body;
  console.log(ctx.request);
  if (!body?.entryPoint) {
    ctx.body = `Please provide an entryPoint`;
    return;
  }
  const entryPoint = body.entryPoint;
  const ssl = body.ssl ?? true;

  const job: JobType = {
    id: `job:${entryPoint}`,
    entryPoint,
    initiatedOn: new Date(),
    ssl,
    state: "running",
  };

  if (await jobExists(job)) {
    ctx.body = `The url ${entryPoint} is already processed`;
    return;
  }

  await setJob(job);

  const url = job.ssl
    ? `https://${job.entryPoint}`
    : `http://${job.entryPoint}`;

  const crawler = await crawlersQueue
    .createJob({ url, jobId: job.id })
    .setId(url)
    .save();

  console.log("added", job, crawler.id);

  ctx.body = "added";
});
