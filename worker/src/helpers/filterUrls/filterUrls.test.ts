import filterUrls from "./filterUrls";

describe("filterUrls()", () => {
  describe("It will filter all unwanted urls", () => {
    const base = new URL("http://example.com/base");
    const urls = [
      "/ok-1",
      "http://example.com/ok-2",
      "ok-3",
      "http://no-example.nl/nok",
      "//example.com/ok-4",
    ];

    const filteredUrls = filterUrls(urls, base);

    it("Only the correct urls remain", () => {
      expect(filteredUrls).toEqual([
        "http://example.com/ok-1",
        "http://example.com/ok-2",
        "http://example.com/ok-3",
        "http://example.com/ok-4",
      ]);
    });
  });

  describe("It will remove the hash", () => {
    const base = new URL("http://example.com/base");
    const urls = [
      "/test-1#hash",
      "test-2#hash",
      "http://example.com/test-3#hash",
    ];

    const filteredUrls = filterUrls(urls, base);

    it("No hashes", () => {
      expect(filteredUrls).toEqual([
        "http://example.com/test-1",
        "http://example.com/test-2",
        "http://example.com/test-3",
      ]);
    });
  });

  describe("It will remove the query params", () => {
    const base = new URL("http://example.com/base");
    const urls = [
      "/test-1?query",
      "test-2?query&query2",
      "http://example.com/test-3?query",
    ];

    const filteredUrls = filterUrls(urls, base);

    it("No hashes", () => {
      expect(filteredUrls).toEqual([
        "http://example.com/test-1",
        "http://example.com/test-2",
        "http://example.com/test-3",
      ]);
    });
  });
});
