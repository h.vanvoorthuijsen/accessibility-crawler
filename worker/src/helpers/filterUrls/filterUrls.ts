const filterUrls = (urls: (string | null)[], base: URL): string[] => {
  const urlsWithoutEmpty = urls.filter(isUrl);

  const asUrlObjects = urlsWithoutEmpty.map((url) => new URL(url, base.origin));

  const withoutExternalLinks = asUrlObjects.filter(
    (url) => url.origin === base.origin
  );

  const asStrings = withoutExternalLinks.map((url) => {
    url.hash = "";
    url.search = "";
    return url.href;
  });

  const unique = [...new Set(asStrings)];

  return unique;
};

const isUrl = (url: string | null): url is string => !!url;

export default filterUrls;
