import Queue from "bee-queue";

const crawlersQueue = new Queue<CrawlerQueueDataType>("crawlers", {
  redis: {
    host: "redis",
  },
  isWorker: false,
});

const watch = async () => {
  /**
    We need to see if the requests for a specific job have settled
    1. See if there are waiting or active items, if not, every job is done!
    2. Get all waiting and active items and check the jobId to see which jobs are done
   */
  console.log("Counts:", await crawlersQueue.checkHealth());
  process.exit();
};

watch();
