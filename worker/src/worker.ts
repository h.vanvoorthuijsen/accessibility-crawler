import AxePuppeteer from "axe-puppeteer";
import Queue from "bee-queue";
import puppeteer from "puppeteer";
import filterUrls from "./helpers/filterUrls";
import fs from "fs/promises";
import slugify from "slugify";

const crawlersQueue = new Queue<CrawlerQueueDataType>("crawlers", {
  redis: {
    host: "redis",
  },
});

crawlersQueue.process(async (job) => {
  console.log("processing", job.id);
  try {
    const baseUrl = new URL(job.data.url);

    const browser = await puppeteer.launch({
      executablePath: "/usr/bin/chromium-browser",
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();
    const response = await page.goto(job.data.url);
    const statusCode = response?.status();

    if (statusCode !== 200) {
      console.log(`The browser responded with status code ${statusCode}`);
      browser.close();
      return;
    }

    const links = await page.$$eval<(string | null)[]>("a", (elements) =>
      elements.map((element) => element.getAttribute("href"))
    );

    const newLinks = filterUrls(links, baseUrl);

    for (const link of newLinks) {
      const url = link.toString();
      crawlersQueue.createJob({ url, jobId: job.data.jobId }).setId(url).save();
    }

    const results = await new AxePuppeteer(page)
      .withTags(["wcag2a", "wcag2aa"])
      .analyze();

    const hostSlug = slugify(
      baseUrl.host.replace(/\./g, "-").replace(/\//g, "__"),
      { remove: /[*+~.()'"!:@]/g }
    );
    const pathSlug = slugify(
      baseUrl.pathname.replace(/\./g, "-").replace(/\//g, "__"),
      { remove: /[*+~.()'"!:@]/g }
    );

    /**
      Lets store this in redis
      Store results in hash: result:$jobId:$url -> just the JSON.stringify of the violations
      Store result key in set: results:$jobId
     */

    await fs.mkdir(`/app/results/${hostSlug}`, { recursive: true });

    await fs.writeFile(
      `/app/results/${hostSlug}/${pathSlug}.json`,
      JSON.stringify(results.violations)
    );

    console.log(job.id, "violations", results.violations.length);

    await browser.close();
  } catch (error) {
    console.error(error);
  }
  return `processed ${job.id}`;
});

process.on("SIGINT", async () => {
  try {
    await crawlersQueue.close(100);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
});
