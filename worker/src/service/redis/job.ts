import { createClient } from "redis";

const redisClient = createClient({ host: "redis" });

export const setJob = async (job: JobType): Promise<boolean> =>
  new Promise((resolve, reject) =>
    redisClient.hset(
      job.id,
      "entryPoint",
      job.entryPoint,
      "initiatedOn",
      job.initiatedOn.toISOString(),
      "ssl",
      job.ssl ? "1" : "0",
      "state",
      job.state,
      (error, nrOfSetKeys) =>
        error ? reject(error) : resolve(nrOfSetKeys == 2)
    )
  );

export const jobExists = async (job: JobType): Promise<boolean> =>
  await new Promise((resolve, reject) =>
    redisClient.exists(job.id, (error, nrOfFoundKeys) =>
      error ? reject(error) : resolve(nrOfFoundKeys > 0)
    )
  );

export const getJob = async (jobId: string): Promise<JobType> =>
  await new Promise((resolve, reject) =>
    redisClient.hgetall(jobId, (error, values) =>
      error
        ? reject(error)
        : resolve({
            id: jobId,
            entryPoint: values.entryPoint,
            state: <JobType["state"]>values.state,
            initiatedOn: new Date(values.initiatedOn),
            ssl: values.ssl === "1" ? true : false,
          })
    )
  );
