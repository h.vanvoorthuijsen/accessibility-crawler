type CrawlerQueueDataType = {
  url: string;
  jobId: string;
};

type JobType = {
  initiatedOn: Date;
  entryPoint: string;
  id: string;
  ssl: boolean;
  state: "running" | "finished";
};
