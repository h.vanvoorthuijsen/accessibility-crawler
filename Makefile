.PHONY: all

# ===========================
# Default: help section
# ===========================

info: intro commands
intro:
	@echo "   _____ _______      __  ____  _    _  __          ________ ____   _____ _____ _______ ______  _____ "
	@echo "  / ____|  __ \ \    / / |  _ \| |  | | \ \        / /  ____|  _ \ / ____|_   _|__   __|  ____|/ ____|"
	@echo " | |    | |__) \ \  / /  | |_) | |  | |  \ \  /\  / /| |__  | |_) | (___   | |    | |  | |__  | (___  "
	@echo " | |    |  _  / \ \/ /   |  _ <| |  | |   \ \/  \/ / |  __| |  _ < \___ \  | |    | |  |  __|  \___ \ "
	@echo " | |____| | \ \  \  /    | |_) | |__| |    \  /\  /  | |____| |_) |____) |_| |_   | |  | |____ ____) |"
	@echo "  \_____|_|  \_\  \/     |____/ \____/      \/  \/   |______|____/|_____/|_____|  |_|  |______|_____/ "

# ===========================
# Main commands
# ===========================

# Project commands
init: intro \
	do-build-containers \
	do-updates \
	do-start-containers
update: intro \
	do-start-containers \
	do-updates

# Docker containers
build: intro do-build-containers do-start-containers
start: intro do-check-hosts-file do-start-proxy do-start-containers do-connect-proxy
stop: intro do-disconnect-proxy do-stop-containers
down: intro do-disconnect-proxy do-bring-containers-down
restart: intro do-stop-containers do-start-containers

# Proxy
stop-hosts-proxy: do-stop-proxy


# ===========================
# Snippets
# ===========================

set-ids = USERID=$$(id -u) GROUPID=$$(id -g)
docker-compose-run = docker-compose run --rm -u $$(id -u):$$(id -g)
docker-compose-exec = docker-compose exec -u $$(id -u):$$(id -g)

# ===========================
# Common recipes
# ===========================

commands:
	@echo "\n=== Makefile commands ===\n"
	@echo "Project commands:"
	@echo "    make init                      Initialise and run the project for development."
	@echo "    make update                    Update the project to run with the latest updates/builds."
	@echo "\nDocker containers:"
	@echo "    make build                     Build and start the project containers."
	@echo "    make start                     Start the project containers."
	@echo "    make stop                      Stop the project containers."
	@echo "    make down                      Stop the project containers and clean the docker-compose environment."
	@echo "    make restart                   Restart the project containers."
	@echo "\nHost proxy"
	@echo "    make stop-hosts-proxy          Stop the hosts proxy"

do-start-containers:
	@echo "\n=== Starting project containers ===\n"
	@${set-ids} docker-compose up -d
	@echo "\nContainers are running!"

do-stop-containers:
	@echo "\n=== Stopping project containers ===\n"
	@${set-ids} docker-compose stop

do-bring-containers-down:
	@echo "\n=== Stopping project containers and cleaning docker-compose environment ===\n"
	@${set-ids} docker-compose down

do-build-containers:
	@echo "\n=== Building project containers ===\n"
	${set-ids} docker-compose build

do-install-git-hooks:
	@echo "\n=== Installing git hooks ===\n"
	cp dev/git-hooks/* .git/hooks
	chmod +x .git/hooks/*

# ===========================
# Update commands
# ===========================

do-updates: do-worker-updates

do-worker-updates:
	@${docker-compose-run} worker npm install
	@${docker-compose-run} worker npm run build

# ===========================
# Hosts proxy
# ===========================

do-start-proxy:
	@echo "\n=== Start hosts proxy ===\n"
	@curl --silent https://gitlab.enrise.com/Enrise/DevProxy/-/raw/master/start.sh | sh

do-connect-proxy:
	@echo "\n=== Connect to hosts proxy ===\n"
	@docker network connect crawler_network enrise-dev-proxy && echo "Connected." || true

do-stop-proxy:
	@echo "\n=== Stop hosts proxy ===\n"
	@docker container stop enrise-dev-proxy && echo "Stopped." || true

do-disconnect-proxy:
	@echo "\n=== Disconnect from hosts proxy ===\n"
	@docker network disconnect crawler_network enrise-dev-proxy && echo "Disconnected." || true

do-check-hosts-file:
	@cat /etc/hosts | grep api.accessibility.crawler > /dev/null \
	|| (echo "\n=== HOSTS MISSING ===\n\n\
	You are missing some hosts in your /etc/hosts file" \
	&& false)
